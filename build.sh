#!/bin/bash

# You may have to install the `zip` command

git clone --depth 1 https://github.com/ProtonMail/proton-bridge
git clone --depth 1 https://github.com/ProtonMail/go-proton-api

# Patch the API
patch -u go-proton-api/manager_builder.go -i patches/manager_builder.patch
# To make the patch itself, run this command once you've made the change:
# diff -u go-proton-api/manager_builder.go go-proton-api/manager_builder_patched.go > manager_builder.patch

CWD=$(pwd)
cd proton-bridge

# Replace the API
go mod edit -replace "github.com/ProtonMail/go-proton-api=$CWD/go-proton-api"

# Build
make build-nogui # (gui doesn't compile on arm64)

# Then run the `bridge` binary in the proton-bridge/ folder.