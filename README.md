This is a script to compile a headless build (no gui) of the [Proton Mail Bridge](https://github.com/ProtonMail/proton-bridge) app, but with a patch to a library to allow it to be used on non-paid accounts.

The MIT license is here only because the patch has the code from the original repo, this script I wrote is public domain.